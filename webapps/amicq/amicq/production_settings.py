# Settings file for the production environment.
# It essentially consist in whatever parameters i need to have different from the ones
# in a development environment. Modify accordingly.

PRODUCTION = True # Don't TOUCH this one, please !

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'amicq$amicqpay',
        'HOST': 'amicq.mysql.pythonanywhere-services.com',
        'USER':'amicq',
        'PASSWORD':'$h1n$3k@1',
        'PORT':''
    }
}
