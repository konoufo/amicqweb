# README #

### À quoi sert ce repositoire ? ###

Ce repositioire contient le code des applications web de l'AMICQ.
Il permet l'accès et l'édition en collaboration.

### Comment être prêt à développer  ###

* Faire un pull avec git pour récupérer le projet sur sa machine.
* Installer Python 2.x (x correspond à la version. Installer le dernier 2.x au mieux)
* Installer les librairies et dépendences:
pip install -r requirements.txt


### Les règles de contribution ###
**À définir**

### Besoin d'aide ? ###

** Contacter au besoin:** 
konoufo@hotmail.fr